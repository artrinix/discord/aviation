plugins {
    kotlin("jvm") version "2.1.10"
    `maven-publish`
    signing
}

group = "xyz.artrinix"
version = System.getenv("CI_COMMIT_SHORT_SHA") ?: "SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://jitpack.io/")
}

dependencies {
    api(libs.bundles.jda) {
        exclude(module = "opus-java")
    }
    api(libs.kotlinx.coroutines)
    api(libs.slf4j.api)
    compileOnly(kotlin("stdlib-common"))
    compileOnly(kotlin("reflect"))
    implementation(libs.reflections)
}

java {
    withJavadocJar()
    withSourcesJar()
}

kotlin {
    jvmToolchain(17)
}

tasks.jar {
    manifest {
        attributes(
            mapOf(
                "Implementation-Title" to project.name,
                "Implementation-Version" to project.version
            )
        )
    }
}

publishing {
    repositories {
        maven {
            name = "GitLab"
            url = uri("${System.getenv("CI_API_V4_URL")}/projects/${System.getenv("CI_PROJECT_ID")}/packages/maven")

            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }

            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }

    publications {
        create<MavenPublication>("aviation") {
            groupId = "xyz.artrinix"
            artifactId = "aviation"
            version = project.version as String

            from(components["java"])

            pom {
                name.set("Aviation")
                description.set("A custom JDA bot framework.")
                url.set("https://gitlab.com/artrinix/discord/aviation")
                licenses {
                    license {
                        name.set("Attribution-NonCommercial-ShareAlike 4.0 International")
                        url.set("https://creativecommons.org/licenses/by-nc-sa/4.0/")
                    }
                }
                developers {
                    developer {
                        id.set("CephalonCosmic")
                        name.set("Storm")
                    }
                }
            }
        }
    }
}