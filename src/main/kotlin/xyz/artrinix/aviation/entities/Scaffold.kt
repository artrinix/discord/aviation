package xyz.artrinix.aviation.entities

import xyz.artrinix.aviation.command.Context
import xyz.artrinix.aviation.internal.entities.ICommand

interface Scaffold {
    fun name(): String? = null

    /**
     * Invoked when an error occurs during command execution.
     * This is local to the scaffold, allowing for per-scaffold error handling.
     *
     * @return Whether the error was handled or not. If it wasn't,
     *         the error will be passed back to the registered
     *         CommandClientAdapter for handling.
     */
    suspend fun onCommandError(ctx: Context, command: ICommand, error: Throwable): Boolean = false

    /**
     * Invoked before a command is executed. This check is local to
     * all commands inside the scaffold.
     *
     * @return Whether the command execution should continue or not.
     */
    suspend fun localCheck(ctx: Context, command: ICommand): Boolean = true
}