package xyz.artrinix.aviation.entities

import xyz.artrinix.aviation.command.Context
import xyz.artrinix.aviation.internal.entities.ICommand

typealias Inhibitor = suspend (Context, ICommand) -> Boolean
