package xyz.artrinix.aviation.exceptions

import xyz.artrinix.aviation.internal.arguments.CommandArgument

class BadArgument(
    val argument: CommandArgument,
    val providedArgument: String?,
    val original: Throwable? = null
) : Throwable("`${argument.name}` must be a `${argument.type.simpleName}`", original)