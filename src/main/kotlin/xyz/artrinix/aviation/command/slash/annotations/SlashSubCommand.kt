package xyz.artrinix.aviation.command.slash.annotations

import net.dv8tion.jda.api.Permission

/**
 * Marks a function as a slash sub-command.
 *
 * Sub-commands cannot co-exist with base commands (marked with @SlashCommand).
 * If a scaffold contains multiple parent commands, and any sub commands, an exception will be thrown.
 *
 * Ideally, commands that have sub commands should be separated into their own scaffolds.
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class SlashSubCommand(
    val description: String = "No description available.",
    val botPermissions: Array<Permission> = []
)
