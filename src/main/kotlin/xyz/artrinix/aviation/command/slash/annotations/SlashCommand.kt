package xyz.artrinix.aviation.command.slash.annotations

import net.dv8tion.jda.api.Permission
import xyz.artrinix.aviation.entities.Scaffold

/**
 * Marks a function within a [Scaffold] as a slash command.
 * Aviation will then detect this and register it.
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
annotation class SlashCommand(
    val name: String = "",
    val description: String = "No description provided",
    val developerOnly: Boolean = false,
    val guildOnly: Boolean = false,
    val defaultUserPermissions: Array<Permission> = [],
    val botPermissions: Array<Permission> = [],
    val guildId: Long = -1L
)
