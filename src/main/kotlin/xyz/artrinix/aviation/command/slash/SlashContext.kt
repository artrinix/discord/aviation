package xyz.artrinix.aviation.command.slash

import dev.minn.jda.ktx.coroutines.await
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.*
import net.dv8tion.jda.api.entities.channel.middleman.GuildChannel
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel
import net.dv8tion.jda.api.interactions.commands.CommandInteraction
import net.dv8tion.jda.api.utils.FileUpload
import xyz.artrinix.aviation.Aviation
import xyz.artrinix.aviation.command.Context
import xyz.artrinix.aviation.entities.Attachment
import xyz.artrinix.aviation.internal.entities.ICommand

class SlashContext(
    val interaction: CommandInteraction,
    override val aviation: Aviation,
    override val command: ICommand.Slash
) : Context {
    override val jda: JDA
        get() = interaction.jda

    override val author: User
        get() = interaction.user

    override val guild: Guild?
        get() = interaction.guild

    override val channel: MessageChannel
        get() = interaction.messageChannel

    override val guildChannel: GuildChannel
        get() = interaction.guildChannel

    override val prefix: String
        get() = "/"

    override val trigger: String
        get() = command.name

    override val member: Member?
        get() = interaction.member

    override suspend fun send(content: String) {
        interaction
            .reply(content)
            .await()
    }

    override suspend fun send(attachment: Attachment) {
        interaction
            .deferReply()
            .addFiles(FileUpload.fromData(attachment.stream, attachment.filename))
            .await()
    }

    override suspend fun sendEmbed(build: EmbedBuilder.() -> Unit) {
        val embed = EmbedBuilder()
            .apply(build)
            .build()

        interaction.replyEmbeds(embed).await()
    }

    override suspend fun sendPrivate(content: String) {
        interaction.deferReply(true)
            .setContent(content)
            .await()
    }

    override suspend fun sendPrivateEmbed(build: EmbedBuilder.() -> Unit) {
        val embed = EmbedBuilder()
            .apply(build)
            .build()

        interaction.deferReply(true)
            .addEmbeds(embed)
            .await()
    }

    override suspend fun sendPrivateFile(attachment: Attachment) {
        interaction.deferReply(true)
            .addFiles(FileUpload.fromData(attachment.stream, attachment.filename))
            .await()
    }
}