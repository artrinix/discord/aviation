package xyz.artrinix.aviation.command.slash.annotations

/**
 * Marks a scaffold as a sub-command holder, this scaffold may only have sub-commands and nested sub-command holders
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class SubCommandHolder(val name: String, val description: String = "No description supplied.")
