package xyz.artrinix.aviation.command

import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.*
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel
import net.dv8tion.jda.api.entities.channel.middleman.GuildChannel
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel
import xyz.artrinix.aviation.Aviation
import xyz.artrinix.aviation.entities.Attachment
import xyz.artrinix.aviation.internal.entities.ICommand

interface Context {
    /**
     * The main Aviation instance that probably created this context.
     */
    val aviation: Aviation

    /**
     * The current JDA instance.
     */
    val jda: JDA

    /**
     * The phrase that triggered the [command] to be executed.
     */
    val trigger: String

    /**
     * The prefix the [author] used to execute the [command]
     */
    val prefix: String

    /**
     * The command that was executed.
     */
    val command: ICommand

    /**
     * The user that executed the [command]
     */
    val author: User

    /**
     * The member that executed the [command], or null if this command was executed in a private channel.
     */
    val member: Member?

    /**
     * The guild that the [command] was executed in, or null if the command was executed in a private channel.
     */
    val guild: Guild?

    /**
     * The channel that [command] was executed in.
     */
    val channel: MessageChannel

    /**
     * The guild channel that [command] was executed in if available.
     */
    val guildChannel: GuildChannel

    /**
     * Returns the [channel] as [TextChannel] if possible\
     */
    val textChannel: TextChannel?
        get() = channel as? TextChannel

    /**
     * Sends a message to the channel the Context was created from.
     */
    suspend fun send(content: String)

    /**
     * Sends a file to the channel the Context was created from.
     *
     * @param attachment
     * The attachment to send.
     */
    suspend fun send(attachment: Attachment)

    /**
     * Sends a [MessageEmbed] to the channel the Context was created from.
     *
     * @param build
     * Options to apply the embed builder.
     */
    suspend fun sendEmbed(build: EmbedBuilder.() -> Unit)

    /**
     * Sends the message author a private message.
     *
     * @param content
     *        The content of the message.
     */
    suspend fun sendPrivate(content: String)

    /**
     * Sends a private [MessageEmbed] to the author.
     *
     * @param build
     *        Options to apply to the message embed.
     */
    suspend fun sendPrivateEmbed(build: EmbedBuilder.() -> Unit)

    /**
     * Sends a private [Attachment] to the author.
     * @param attachment
     * The attachment to send.
     */
    suspend fun sendPrivateFile(attachment: Attachment)
}