package xyz.artrinix.aviation.command.message

import dev.minn.jda.ktx.coroutines.await
import kotlinx.coroutines.future.await as coroutineAwait
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.*
import net.dv8tion.jda.api.entities.channel.ChannelType
import net.dv8tion.jda.api.entities.channel.concrete.PrivateChannel
import net.dv8tion.jda.api.entities.channel.middleman.GuildMessageChannel
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel
import net.dv8tion.jda.api.utils.FileUpload
import xyz.artrinix.aviation.Aviation
import xyz.artrinix.aviation.command.Context
import xyz.artrinix.aviation.entities.Attachment
import xyz.artrinix.aviation.internal.entities.ICommand
import xyz.artrinix.aviation.internal.utils.Scheduler
import java.util.regex.Pattern

class MessageContext(
    override val aviation: Aviation,
    val message: Message,
    override val trigger: String,
    override val command: ICommand,
    override val prefix: String,
    val args: List<String>
) : Context {
    companion object {
        private val mentionPattern = Pattern.compile("(?<mention><(?<type>@!?|@&|#)(?<id>[0-9]{17,21})>)")
    }

    override val jda: JDA
        get() = message.jda

    override val author: User
        get() = message.author

    override val guild: Guild?
        get() = if (message.isFromGuild) message.guild else null

    override val channel: MessageChannel
        get() = message.channel

    override val guildChannel: GuildMessageChannel
        get() = message.guildChannel

    override val member: Member?
        get() = message.member

    val privateChannel: PrivateChannel? =
        if (message.isFromType(ChannelType.PRIVATE)) message.channel.asPrivateChannel() else null

    override suspend fun send(content: String) {
        channel.sendMessage(content).await()
    }

    override suspend fun send(attachment: Attachment) {
        channel.sendFiles(FileUpload.fromData(attachment.stream, attachment.filename)).await()
    }

    override suspend fun sendEmbed(build: EmbedBuilder.() -> Unit) {
        val embed = EmbedBuilder()
            .apply(build)
            .build()

        channel.sendMessageEmbeds(embed).await()
    }

    override suspend fun sendPrivate(content: String) {
        author.openPrivateChannel().await()
            .also {
                it.sendMessage(content).await()
                it.delete().await()
            }
    }

    override suspend fun sendPrivateEmbed(build: EmbedBuilder.() -> Unit) {
        author.openPrivateChannel().await()
            .also {
                val embed = EmbedBuilder()
                    .apply(build)
                    .build()

                it.sendMessageEmbeds(embed).await()
                it.delete().await()
            }
    }

    override suspend fun sendPrivateFile(attachment: Attachment) {
        author.openPrivateChannel().await()
            .also {
                it.sendFiles(FileUpload.fromData(attachment.stream, attachment.filename)).await()
                it.delete().await()
            }
    }

    /**
     * Sends a typing status within the channel until the provided function is exited.
     *
     * @param block
     * The code that should be executed while the typing status is active.
     */
    suspend fun typing(block: suspend () -> Unit) {
        channel.sendTyping().submit().coroutineAwait()

        val task = Scheduler.every(5000) { channel.sendTyping().queue() }
        try {
            block()
        } finally {
            task.cancel(true)
        }
    }
}