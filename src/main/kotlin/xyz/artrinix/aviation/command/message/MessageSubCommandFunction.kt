package xyz.artrinix.aviation.command.message

import xyz.artrinix.aviation.command.message.annotations.MessageSubCommand
import xyz.artrinix.aviation.entities.Scaffold
import xyz.artrinix.aviation.internal.arguments.CommandArgument
import xyz.artrinix.aviation.internal.entities.ICommand
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter

data class MessageSubCommandFunction(
    override val name: String,
    val properties: MessageSubCommand,
    // Executable properties
    override val method: KFunction<*>,
    override val scaffold: Scaffold,
    override val arguments: List<CommandArgument>,
    override val contextParameter: KParameter
) : ICommand.Message
