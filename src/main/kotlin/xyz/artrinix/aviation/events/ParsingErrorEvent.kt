package xyz.artrinix.aviation.events

import xyz.artrinix.aviation.command.Context
import xyz.artrinix.aviation.internal.entities.ICommand

/**
 * Emitted when the argument parser encounters an error.
 */
data class ParsingErrorEvent(
    val ctx: Context,
    val command: ICommand,
    val error: Throwable
) : Event
