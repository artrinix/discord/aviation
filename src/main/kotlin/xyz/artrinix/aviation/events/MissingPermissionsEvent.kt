package xyz.artrinix.aviation.events

import net.dv8tion.jda.api.Permission
import xyz.artrinix.aviation.command.Context
import xyz.artrinix.aviation.internal.entities.ICommand

/**
 * Emitted when we're lacking permissions to execute a command.
 *
 * @param ctx
 *   The current command context.
 *
 * @param command
 *   The command
 *
 * @param permissions
 *   List of [Permission]s we are lacking.
 */
data class MissingPermissionsEvent(val ctx: Context, val command: ICommand, val permissions: List<Permission>) : Event
