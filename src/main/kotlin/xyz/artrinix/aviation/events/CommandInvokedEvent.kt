package xyz.artrinix.aviation.events

import xyz.artrinix.aviation.command.Context
import xyz.artrinix.aviation.internal.entities.ICommand

/**
 * Emitted before a command is executed. Useful logging command usage etc.
 *
 * @param ctx
 *   The command context
 *
 * @param command
 *   The command being executed.
 */
data class CommandInvokedEvent(val ctx: Context, val command: ICommand) : Event
