package xyz.artrinix.aviation

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableSharedFlow
import xyz.artrinix.aviation.entities.DescriptionProvider
import xyz.artrinix.aviation.entities.Inhibitor
import xyz.artrinix.aviation.entities.PrefixProvider
import xyz.artrinix.aviation.events.Event
import xyz.artrinix.aviation.ratelimit.RateLimitStrategy

data class AviationResources(
    val prefixes: PrefixProvider,
    val ratelimits: RateLimitStrategy,
    val ignoreBots: Boolean,
    val dispatcher: CoroutineDispatcher,
    val eventFlow: MutableSharedFlow<Event>,
    val doTyping: Boolean,
    val inhibitor: Inhibitor,
    val developers: MutableSet<Long>,
    val testGuilds: MutableSet<Long>,
    val descriptionProvider: DescriptionProvider
)
