package xyz.artrinix.aviation.ratelimit

enum class RateLimitType {
    USER,
    GUILD,
    GLOBAL,
    CHANNEL
}
