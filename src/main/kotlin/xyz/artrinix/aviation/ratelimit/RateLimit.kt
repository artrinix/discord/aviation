package xyz.artrinix.aviation.ratelimit

import xyz.artrinix.aviation.command.message.MessageContext
import xyz.artrinix.aviation.annotations.RateLimit

data class RateLimit(val name: String, val expiresAt: Long) {
    companion object {
        fun getEntityId(options: RateLimit, ctx: MessageContext): Long? = when (options.type) {
            RateLimitType.USER -> ctx.author.idLong
            RateLimitType.GUILD -> ctx.guild?.idLong
            RateLimitType.CHANNEL -> ctx.channel.idLong
            RateLimitType.GLOBAL -> -1
        }
    }
}
