package xyz.artrinix.aviation.internal.entities

import net.dv8tion.jda.api.interactions.commands.Command
import net.dv8tion.jda.api.requests.RestAction
import net.dv8tion.jda.api.requests.restaction.CommandListUpdateAction

interface ICommandHolder {
    fun retrieveCommands(): RestAction<List<Command>>
    fun updateCommands(): CommandListUpdateAction
}