package xyz.artrinix.aviation.internal.entities

import xyz.artrinix.aviation.entities.AbstractModule
import xyz.artrinix.aviation.internal.utils.Indexer

class ModuleRegistry : HashMap<String, AbstractModule>() {
    inline fun <reified T> findModuleByClass(): T? {
        return this.values.firstOrNull { it is T } as T
    }

    fun findModuleByName(name: String): AbstractModule? {
        return this.values.firstOrNull { it::class.simpleName == name }
    }

    suspend fun register(packageName: String) {
        val indexer = Indexer(packageName)
        for (module in indexer.getModules()) register(module)
    }

    suspend fun register(module: AbstractModule) {
        this[module::class.simpleName ?: module::class.java.simpleName] = module
        module.onEnable()
    }

    suspend fun unload(module: AbstractModule) {
        module.onDisable()
        this.values.remove(module)
    }
}