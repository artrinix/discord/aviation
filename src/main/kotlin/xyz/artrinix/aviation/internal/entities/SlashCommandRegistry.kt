package xyz.artrinix.aviation.internal.entities

import xyz.artrinix.aviation.command.slash.SlashCommandFunction
import xyz.artrinix.aviation.entities.Scaffold
import xyz.artrinix.aviation.internal.utils.Indexer
import xyz.artrinix.aviation.internal.utils.SlashCommandUtil

class SlashCommandRegistry : HashMap<String, SlashCommandFunction>() {
    fun findCommandByName(name: String): SlashCommandFunction? {
        return this[name]
    }

    fun findScaffoldByName(name: String): Scaffold? {
        return this.values.firstOrNull { it.scaffold::class.simpleName == name }?.scaffold
    }

    fun findCommandsByScaffold(scaffold: Scaffold): List<SlashCommandFunction> {
        return this.values.filter { it.scaffold == scaffold }
    }

    fun unload(commandFunction: SlashCommandFunction) {
        this.values.remove(commandFunction)
    }

    fun unload(scaffold: Scaffold) {
        val commands = this.values.filter { it.scaffold == scaffold }
        this.values.removeAll(commands)
    }

    fun register(packageName: String) {
        val indexer = Indexer(packageName)
        for (scaffold in indexer.getScaffolds()) {
            register(scaffold)
        }
    }

    fun register(scaffold: Scaffold) {
        val commands = SlashCommandUtil.loadScaffold(scaffold)
        for (command in commands) {
            if (this.containsKey(command.name)) {
                throw RuntimeException("Cannot register command ${command.name}; the trigger has already been registered.")
            }

            this[command.name] = command
        }
    }
}
