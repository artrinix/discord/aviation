package xyz.artrinix.aviation.internal.entities

import xyz.artrinix.aviation.command.message.MessageCommandFunction
import xyz.artrinix.aviation.entities.Scaffold
import xyz.artrinix.aviation.internal.utils.Indexer
import xyz.artrinix.aviation.internal.utils.MessageCommandUtil

class CommandRegistry : HashMap<String, MessageCommandFunction>() {
    fun findCommandByName(name: String): MessageCommandFunction? {
        return this.get(name)
    }

    fun findCommandByAlias(alias: String): MessageCommandFunction? {
        return this.values.firstOrNull { it.properties.aliases.contains(alias) }
    }

    fun findScaffoldByName(name: String): Scaffold? {
        return this.values.firstOrNull { it.scaffold::class.simpleName == name }?.scaffold
    }

    fun findCommandsByScaffold(scaffold: Scaffold): List<MessageCommandFunction> {
        return this.values.filter { it.scaffold == scaffold }
    }

    fun unload(commandFunction: MessageCommandFunction) {
        this.values.remove(commandFunction)
    }

    fun unload(scaffold: Scaffold) {
        val commands = this.values.filter { it.scaffold == scaffold }.toSet()
        this.values.removeAll(commands)
    }

    fun register(packageName: String) {
        val indexer = Indexer(packageName)
        for (scaffold in indexer.getScaffolds()) {
            register(scaffold, indexer)
        }
    }

    fun register(scaffold: Scaffold, indexer: Indexer? = null) {
        val i = indexer ?: Indexer(scaffold::class.java.`package`.name)
        val commands = i.getMessageCommands(scaffold)

        for (command in commands) {
            val cmd = MessageCommandUtil.loadCommand(command, scaffold)

            if (this.containsKey(cmd.name)) {
                throw RuntimeException("Cannot register command ${cmd.name}; the trigger has already been registered.")
            }

            this[cmd.name] = cmd
        }
    }
}
