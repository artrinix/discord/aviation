package xyz.artrinix.aviation.internal.entities

typealias ExecutionCallback = suspend (Boolean, Throwable?) -> Unit