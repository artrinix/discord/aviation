package xyz.artrinix.aviation.internal.utils

import org.slf4j.LoggerFactory
import xyz.artrinix.aviation.annotations.Name
import xyz.artrinix.aviation.command.Context
import xyz.artrinix.aviation.command.slash.SlashCommandFunction
import xyz.artrinix.aviation.command.slash.SlashSubCommandFunction
import xyz.artrinix.aviation.command.slash.SubCommandGroup
import xyz.artrinix.aviation.command.slash.annotations.Description
import xyz.artrinix.aviation.command.slash.annotations.SlashCommand
import xyz.artrinix.aviation.command.slash.annotations.SlashSubCommand
import xyz.artrinix.aviation.command.slash.annotations.SubCommandHolder
import xyz.artrinix.aviation.entities.Scaffold
import xyz.artrinix.aviation.internal.arguments.CommandArgument
import xyz.artrinix.aviation.internal.entities.ICommand
import kotlin.reflect.KFunction
import kotlin.reflect.KParameter
import kotlin.reflect.full.*
import kotlin.reflect.jvm.jvmErasure
import kotlin.reflect.typeOf

object SlashCommandUtil {
    private val logger = LoggerFactory.getLogger(SlashCommandUtil::class.java)

    fun loadScaffold(scaffold: Scaffold): List<SlashCommandFunction> {
        val holder = scaffold::class.findAnnotation<SlashCommand>()
        if (holder == null) {
            logger.debug("Scanning ${scaffold::class.simpleName} for slash commands...")

            val scaffoldClass = scaffold::class
            val commands = scaffoldClass.members
                .filterIsInstance<KFunction<*>>()
                .filter { it.hasAnnotation<SlashCommand>() }

            logger.debug("Found ${commands.size} slash commands in scaffold ${scaffold::class.simpleName}")
            return commands.map { loadICommand(it, scaffold) as SlashCommandFunction }
        }


        val category = scaffold.name()
            ?: scaffold::class.java.`package`.name.split('.')
                .last()
                .replace('_', ' ')
                .lowercase()
                .replaceFirstChar { it.uppercase() }

        val subCommands = loadSubCommands(scaffold)
        val subCommandGroups = loadSubCommandGroups(scaffold).associateBy { it.name }

        return listOf(SlashCommandFunction(holder.name, null, scaffold, emptyList(), null, category, holder, subCommands, subCommandGroups))
    }

    fun loadSubCommandGroups(scaffold: Scaffold): List<SubCommandGroup> {
        logger.debug("Scanning ${scaffold::class.simpleName} for slash sub-command groups...")

        val groups = mutableListOf<SubCommandGroup>()
        for (nested in scaffold::class.nestedClasses) {
            val holder = nested.findAnnotation<SubCommandHolder>()
                ?: continue

            val subCommands = loadSubCommands(scaffold, nested.createInstance())
            groups.add(SubCommandGroup(holder.name, holder.description, subCommands))
        }

        return groups
    }

    fun loadSubCommands(scaffold: Scaffold, declaringClass: Any = scaffold): List<SlashSubCommandFunction> {
        logger.debug("Scanning ${declaringClass::class.simpleName} for slash sub-commands...")

        val subCommands = declaringClass::class.members
            .filterIsInstance<KFunction<*>>()
            .filter { it.hasAnnotation<SlashSubCommand>() }

        return subCommands.map { loadICommand(it, scaffold, declaringClass) as SlashSubCommandFunction }
    }

    fun loadICommand(meth: KFunction<*>, scaffold: Scaffold, declaringClass: Any = scaffold): ICommand {
        /* get the name of this sub-command */
        val name = meth.findAnnotation<Name>()?.name
            ?: meth.name.lowercase()

        val annotationClass = when {
            meth.hasAnnotation<SlashCommand>() -> "slashCommand"
            meth.hasAnnotation<SlashSubCommand>() -> "slashSubCommand"
            else -> error("${meth.name} is not annotated with Slash(Sub)Command!")
        }

        /* find the slash context parameter. */
        val ctxParam = meth.valueParameters.firstOrNull {
            it.type.isSubtypeOf(typeOf<Context>())
        }

        require(ctxParam != null) {
            "${meth.name} is missing the SlashContext parameter!"
        }

        /* find arguments */
        val parameters = meth.valueParameters.filterNot {
            it.type.isSubtypeOf(typeOf<Context>())
        }

        val arguments = getArguments(parameters)
        return when (annotationClass) {
            "slashSubCommand" -> {
                val properties = meth.findAnnotation<SlashSubCommand>()!!
                SlashSubCommandFunction(name, meth, scaffold, arguments, ctxParam, declaringClass, properties)
            }

            "slashCommand" -> {
                val category = scaffold.name()
                    ?: scaffold::class.java.`package`.name.split('.')
                        .last()
                        .replace('_', ' ')
                        .lowercase()
                        .replaceFirstChar { it.uppercase() }

                val properties = meth.findAnnotation<SlashCommand>()!!
                SlashCommandFunction(name, meth, scaffold, arguments, ctxParam, category, properties, emptyList(), emptyMap())
            }

            else -> error("what")
        }
    }

    fun getArguments(parameters: List<KParameter>): List<CommandArgument> {
        val arguments = mutableListOf<CommandArgument>()
        for (p in parameters) {
            val name = p.findAnnotation<Name>()?.name
                ?: p.name
                ?: p.index.toString()

            val description = p.findAnnotation<Description>()?.value
                ?: "No description provided."

            val commandArgument = CommandArgument(
                name,
                p.type.jvmErasure.javaObjectType,
                null,
                p.isOptional,
                p.type.isMarkedNullable,
                false,
                CommandArgument.Option(description),
                p
            )

            arguments.add(commandArgument)
        }

        return arguments
    }
}
