package xyz.artrinix.aviation.internal.parsers

import net.dv8tion.jda.api.entities.emoji.Emoji
import net.dv8tion.jda.api.interactions.commands.OptionMapping
import net.dv8tion.jda.api.interactions.commands.OptionType
import xyz.artrinix.aviation.command.message.MessageContext
import xyz.artrinix.aviation.command.slash.SlashContext
import java.util.*
import java.util.regex.Pattern

class EmojiResolver : Resolver<Emoji> {
    override val optionType: OptionType = OptionType.STRING

    override suspend fun resolveOption(ctx: SlashContext, option: OptionMapping) =
        resolveEmoji(option.asString)

    override suspend fun resolve(ctx: MessageContext, param: String): Optional<Emoji> =
        resolveEmoji(param)

    companion object {
        val EMOJI_REGEX = Pattern.compile("<(a)?:(\\w+):(\\d{17,21})")!!

        fun resolveEmoji(emoji: String): Optional<Emoji> {
            return try {
                Optional.of(Emoji.fromUnicode(emoji))
            } catch (e: IllegalArgumentException) {
                val match = EMOJI_REGEX.matcher(emoji)

                if (match.find()) {
                    val isAnimated = match.group(1) != null
                    val name = match.group(2)
                    val id = match.group(3).toLong()

                    return Optional.of(Emoji.fromCustom(name, id, isAnimated))
                }

                Optional.empty()
            }
        }
    }
}
