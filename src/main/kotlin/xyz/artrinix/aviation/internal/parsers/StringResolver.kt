package xyz.artrinix.aviation.internal.parsers

import net.dv8tion.jda.api.interactions.commands.OptionMapping
import net.dv8tion.jda.api.interactions.commands.OptionType
import xyz.artrinix.aviation.command.message.MessageContext
import xyz.artrinix.aviation.command.slash.SlashContext
import java.util.*

class StringResolver : Resolver<String> {
    override val optionType: OptionType = OptionType.STRING

    override suspend fun resolveOption(ctx: SlashContext, option: OptionMapping) =
        Optional.of(option.asString)

    override suspend fun resolve(ctx: MessageContext, param: String): Optional<String> {
        if (param.isEmpty() || param.isBlank()) {
            return Optional.empty()
        }

        return Optional.of(param)
    }
}