package xyz.artrinix.aviation.internal.parsers

import net.dv8tion.jda.api.entities.channel.concrete.VoiceChannel
import net.dv8tion.jda.api.interactions.commands.OptionMapping
import net.dv8tion.jda.api.interactions.commands.OptionType
import xyz.artrinix.aviation.command.message.MessageContext
import xyz.artrinix.aviation.command.slash.SlashContext
import java.util.*

class VoiceChannelResolver : Resolver<VoiceChannel> {
    override val optionType: OptionType = OptionType.CHANNEL

    override suspend fun resolveOption(ctx: SlashContext, option: OptionMapping): Optional<VoiceChannel> =
        Optional.ofNullable(option.asChannel.asGuildMessageChannel() as? VoiceChannel)

    override suspend fun resolve(ctx: MessageContext, param: String): Optional<VoiceChannel> {
        val snowflake = SnowflakeResolver.resolve(ctx, param)

        /* test cache */
        val channel: VoiceChannel? = if (snowflake.isPresent) {
            ctx.guild?.getVoiceChannelById(snowflake.get().resolved)
        } else {
            ctx.guild?.voiceChannels?.firstOrNull { it.name == param }
        }

        return Optional.ofNullable(channel)
    }

}