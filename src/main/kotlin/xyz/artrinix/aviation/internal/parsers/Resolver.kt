package xyz.artrinix.aviation.internal.parsers

import net.dv8tion.jda.api.interactions.commands.Command
import net.dv8tion.jda.api.interactions.commands.OptionMapping
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.OptionData
import xyz.artrinix.aviation.command.message.MessageContext
import xyz.artrinix.aviation.command.slash.SlashContext
import xyz.artrinix.aviation.internal.arguments.CommandArgument
import java.util.*
import javax.naming.OperationNotSupportedException

interface Resolver<T> {
    val optionType: OptionType?
        get() = null

    suspend fun getOption(argument: CommandArgument): OptionData {
        optionType ?: throw OperationNotSupportedException()
        val description = argument.optionProperties?.description!!
        return OptionData(optionType!!, argument.name, description, argument.isRequired)
    }

    /**
     * This was supposed to be for the Slash Command Choices, but I never implemented this
     */
    suspend fun getOptionChoice(name: String, value: Any): Command.Choice? = null

    /**
     *
     */
    suspend fun resolveOption(ctx: SlashContext, option: OptionMapping): Optional<T & Any> {
        return Optional.empty<T>()
    }

    /**
     *
     */
    suspend fun resolve(ctx: MessageContext, param: String): Optional<T & Any> {
        return Optional.empty<T>()
    }
}