package xyz.artrinix.aviation.internal.parsers

import net.dv8tion.jda.api.interactions.commands.OptionMapping
import net.dv8tion.jda.api.interactions.commands.OptionType
import xyz.artrinix.aviation.command.message.MessageContext
import xyz.artrinix.aviation.command.slash.SlashContext
import java.net.URI
import java.net.URL
import java.util.*

class UrlResolver : Resolver<URL> {
    override val optionType: OptionType = OptionType.STRING

    override suspend fun resolveOption(ctx: SlashContext, option: OptionMapping): Optional<URL> =
        parse(option.asString)

    override suspend fun resolve(ctx: MessageContext, param: String): Optional<URL> =
        parse(param)

    private fun parse(param: String): Optional<URL> = try {
        Optional.of(URI(param).toURL())
    } catch (e: Throwable) {
        Optional.empty()
    }
}